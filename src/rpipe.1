.\" This file is part of rpipe -*- nroff -*-
.\" Copyright (C) 2019-2021 Sergey Poznyakoff
.\"
.\" Rpipe is free software; you can redistribute it and/or modify
.\" it under the terms of the GNU General Public License as published by
.\" the Free Software Foundation; either version 3, or (at your option)
.\" any later version.
.\"
.\" Rpipe is distributed in the hope that it will be useful,
.\" but WITHOUT ANY WARRANTY; without even the implied warranty of
.\" MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\" GNU General Public License for more details.
.\"
.\" You should have received a copy of the GNU General Public License
.\" along with rpipe.  If not, see <http://www.gnu.org/licenses/>.
.TH RPIPE 1 "May 12, 2021" "RPIPE"
.SH NAME
rpipe \- remote pipe multiplexer
.SH SYNOPSIS
.nh
.na
\fBrpipe\fR\
 [\fB\-sTv\fR]\
 [\fB\-a \fIIPADDR\fR]\
 [\fB\-g \fIGROUP\fR[,\fIGROUP\fR...]]\
 [\fB\-n \fINUMBER\fR]\
 [\fB\-p \fIFILE\fR]\
 [\fB\-S \fIFACILITY\fR]\
 [\fB\-u \fIUSER\fR]\
 \fIARG\fR...
.sp
\fBrpipe \-h\fR | \fB\-V\fR
.ad
.hy
.SH DESCRIPTION
The \fBrpipe\fR tool is a simple TCP multiplexer that makes it
possible to send data over the network to the standard input of
a remote program much as one would do the same with a local
program using shell pipe construct.
.PP
The program operates in three modes: server, client and test.
.PP
In server mode (requested by the \fB\-s\fR command line option), it
listens for TCP requests on the indicated IP address and port
(default: port 1 on all available network interfaces). The first
command line argument is treated as full pathname of the command on
the server and the rest of arguments as initial arguments for that
command.
.PP
Upon receiving an incoming connection, its first line is split into
words and appended to the command line stub constructed as described
above. The resulting command line is then run and the rest of incoming
data is supplied to it as its standard input.
.PP
The server replies with an RFC 1078 compliant response line and
waits for the program to terminate. Any data received over the network
are piped to the program's standard input. The program's standard
output is captured and sent back to the client. During transmission,
each linefeed character (LF, ASCII 10) is replaced by two characters:
newline (CR, ASCII 13) and linefeed. A dot appearing at the beginning of
a line is duplicated. End of program output is marked with a
<CRLF>.<CRLF> marker. When the program terminates, its exit status (as
returned by \fBwait\fR(2)) is formatted in decimal and sent over the
network, followed by <CRLF>.
.PP
In client mode, \fBrpipe\fR sends its arguments as the first line of
the request immediately followed by its standard input. Any data
received from the remote end is first decoded by reverting the
process described above and then reproduced on the standard output.
The command will exit with the same exit status as returned by the
remote program. If an error occurred, it will exit with code 69
(\fBEX_UNAVAILABLE\fR), or 71 (\fBEX_OSERR\fR), depending on the
error.
.PP
The test mode is designed to assist in testing the package.  It is enabled
by the \fB\-T\fR switch.  It this mode, \fBrpipe\fR starts the server
listening on an arbitrary port on localhost and connects to it as
client.  The \fB\-a\fR option is ignored.  Command line arguments must
consist of two parts, delimited by a
.B \-\-
separator.  The part before
.B \-\-
supplies arguments for the server, and the part after it supplies
arguments for the client.
.PP
In other words, the invocation
.nh
.na
.B rpipe -T
.I COMMAND ARG1
.B \-\-
.I ARG2 ARG3
.ad
.hy
is roughly equivalent to:
.sp
.nh
.na
.B rpipe -s
\fB\-a:\fIPORT\fR
.I COMMAND ARG1
&
.br
.B rpipe -s
\fB\-a:\fIPORT\fR
.I ARG2 ARG3
.ad
.hy
.sp
where \fIPORT\fR stands for the first free port on localhost.  When
the program exits, both instances are terminated.
.SH OPTIONS
.TP
\fB\-a \fIIPADDR\fR[:\fIPORT\fR]
In server mode: define IP address and port to listen on. Either
\fIIPADDR\fR or \fIPORT\fR, but not both, can be omitted. If
\fIPORT\fR is omitted, it defaults to 1 (the \fBtcpmux\fR service). In
this case the colon is optional. If \fIIPADDR\fR is omitted the colon is
mandatory.  Missing \fIIPADDR\fR defaults to \fB0.0.0.0\fR.
.sp
If this option is not used, the IP address is obtained from the
environment variable
.BR RPIPE_ADDRESS .
If it is not set, the default
.B 0.0.0.0:1
is used.
.sp
In client mode, this option defines the IP address (and optional port)
to send data to. At least the \fIIPADDR\fR portion must be present.
If not set, the IP address will be obtained from the environment
variable
.BR RPIPE_ADDRESS .
If it is not set, an error is signaled.
.TP
\fB\-g \fIGROUP\fR[,\fIGROUP\fR...]
Valid in server mode only. Run the command with \fIGROUP\fR as the
primary owner group. Any additional \fIGROUP\fRs are set as the
supplementary groups. Each \fIGROUP\fR can be either a symbolic group
name, or a numeric GID preceded by a plus sign.

See also the \fB\-u\fR option.
.TP
\fB\-h\fR
Print a short help summary and exit.
.TP
\fB\-n \fINUMBER\fR
Valid in server mode only. Set maximum number of simultaneous
connections. Default is 128.
.TP
\fB\-p \fIFILE\fR
Valid in server mode only. Read run-time execution privileges for the
command from \fIFILE\fR. Each non-empty line in \fIFILE\fR consists of
two or more words separated by whitespace. The first word is a
subcommand name, the second one is the name of the user to run the
subcommand as, and remaining words are groups to run as. Comments are
introduced with the hash sign and extend up to the end of the
line. Before running the command, the first argument from the command
line obtained from the client is looked up in that file. If found, the
rest of fields gives the privileges to run the constructed command
with. If not found, user and group supplied by \fB\-u\fR and \fB\-g\fR
options (if given) will be used.
.TP
\fB\-S \fIFACILITY\fR
Log messages to the given syslog facility.  \fIFACILITY\fR is one of
.BR auth ,
.BR authpriv ,
.BR cron ,
.BR daemon ,
.BR ftp ,
.BR kern ,
.BR lpr ,
.BR mail ,
.BR news ,
.BR user ,
.BR uucp ,
.BR local0 " through " local7 ,
or a decimal facility number.
.sp
By default all messages are written to stderr.
.TP 
.B \-s
Enable server mode.
.TP
.B \-T
Enable the test mode.
.TP
\fB\-u \fIUSER\fR
In server mode. Run the command with the privileges of
\fIUSER\fR, which is either a symbolic user name or a numeric UID (in
decimal), preceded by a plus sign.
.TP
.B \-V
Print program version and copyright information and exit.
.TP
.B \-v
Increase verbosity level.
.SH ENVIRONMENT VARIABLES
.TP
.B RPIPE_ADDRESS
IP address and optional port to listen to (in server mode), or to
connect to (in client mode).
.SH SECURITY CONSIDERATIONS
The program is intended for deployment in a secure environment. No
authorization is performed. No data encryption is implemented.
.SH AUTHORS
Sergey Poznyakoff
.SH "BUG REPORTS"
Report bugs to <gray@gnu.org.ua>.
.SH COPYRIGHT
Copyright \(co 2019--2021 Sergey Poznyakoff
.br
.na
License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
.br
.ad
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
.\" Local variables:
.\" eval: (add-hook 'write-file-hooks 'time-stamp)
.\" time-stamp-start: ".TH [A-Z_][A-Z0-9_.\\-]* [0-9] \""
.\" time-stamp-format: "%:B %:d, %:y"
.\" time-stamp-end: "\""
.\" time-stamp-line-limit: 20
.\" end:

