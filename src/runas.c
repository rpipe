/* This file is part of rpipe
   Copyright (C) 2018-2021 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later
   <http://gnu.org/licenses/gpl.html>
   
   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <sysexits.h>
#include <stdarg.h>
#include "rpipe.h"

static void
add_gid(runas_t *r, gid_t gid)
{
	if (r->gidc == r->gidn) 
		r->gidv = x2nrealloc(r->gidv, &r->gidn, sizeof(r->gidv[0]));
	r->gidv[r->gidc++] = gid;
}

static void
get_user_groups(runas_t *r, char const *user_name)
{
	struct group *gr;

        setgrent();
        while ((gr = getgrent())) {
                char **p;
                for (p = gr->gr_mem; *p; p++) {
                        if (strcmp(*p, user_name) == 0)
				add_gid(r, gr->gr_gid);
		}
        }
        endgrent();
}

static void
parse_gid_str(runas_t *r, char const *group_name)
{
	gid_t gid;
	
	if (group_name[0] == '+') {
		char *end;
		unsigned long n;

		errno = 0;
		n = strtoul(group_name + 1, &end, 10);
		if (errno || *end)
			error(EX_USAGE, 0, "invalid group name %s", group_name);
		gid = n;
	} else {
		struct group *gr = getgrnam(group_name);
		if (!gr)
			error(EX_USAGE, 0, "%s: no such group", group_name);
		gid = gr->gr_gid;
	}
	
	add_gid(r, gid);
}

static void
parse_gid_bytes(runas_t *r, char const *group_name, size_t len)
{
	char *name;

	name = xmalloc(len + 1);
	memcpy(name, group_name, len);
	name[len] = 0;
	parse_gid_str(r, name);
	free(name);
}

runas_t *
runas_parsev(char const *user_name, char const **grv, size_t grc)
{
	runas_t *r;
	struct passwd *pw;

	r = xcalloc(1, sizeof(*r));

	if (!user_name) {
		pw = getpwuid(0);
		user_name = "root";
	} else if (user_name[0] == '+') {
		char *end;
		unsigned long n;

		errno = 0;
		n = strtoul(user_name + 1, &end, 10);
		if (errno || *end)
			error(EX_USAGE, 0, "invalid user name %s", user_name);
		pw = getpwuid(n);
	} else
		pw = getpwnam(user_name);

	if (!pw)
		error(EX_USAGE, 0, "%s: no such user", user_name);

	user_name = pw->pw_name;
	r->uid = pw->pw_uid;
	r->gid = pw->pw_gid;

	if (grc && grv[0]) {
		size_t i;
		for (i = 0; i < grc; i++) {
			char const *group_names = grv[i];
			do {
				size_t len = strcspn(group_names, ",");
				if (len == 0)
					continue;
				parse_gid_bytes(r, group_names, len);
				group_names += len;
			} while (*group_names++ == ',');
			if (group_names[-1])
				error(EX_USAGE, 0,
				      "junk in group string near %s",
				      group_names - 1);
			r->gid = r->gidv[0];
		}
	} else
		get_user_groups(r, user_name);

	return r;
}

void
runas(runas_t *r)
{
	if (!r)
		return;
	
	/* Reset group permissions */
	if (r->gidv) {
		if (setgroups(r->gidc, r->gidv)) 
			error(EX_OSERR, errno, "setgroups");
	}
	
	/* Switch to the user's gid. */
	if (setregid(r->gid, -1) < 0)
		error(EX_OSERR, errno, "setregid");

	/* Reset uid */
	if (setreuid(r->uid, -1))
		error(EX_OSERR, errno, "setreuid");
}


