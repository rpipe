/* This file is part of rpipe
   Copyright (C) 2018 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later
   <http://gnu.org/licenses/gpl.html>

   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <sysexits.h>
#include <ctype.h>
#include "rpipe.h"

#define S_UN_NAME(sa, salen)					\
	((salen < offsetof(struct sockaddr_un,sun_path)) ?	\
	  "" : (sa)->sun_path)

char *
sockaddr_str(struct sockaddr *sa, int salen)
{
	char buf[512];

	switch (sa->sa_family) {
	case AF_INET:
	case AF_INET6: {
		char host[NI_MAXHOST];
		char srv[NI_MAXSERV];
		if (getnameinfo(sa, salen,
				host, sizeof(host), srv, sizeof(srv),
				NI_NUMERICHOST|NI_NUMERICSERV) == 0)
			snprintf(buf, sizeof buf, "%s://%s:%s",
				 sa->sa_family == AF_INET ?
				   "inet" : "inet6",
				 host, srv);
		else
			snprintf(buf, sizeof buf, "%s://[getnameinfo failed]",
				 sa->sa_family == AF_INET ?
				   "inet" : "inet6");
		break;
	}

	case AF_UNIX: {
		struct sockaddr_un *s_un = (struct sockaddr_un *)sa;
		if (S_UN_NAME(s_un, salen)[0] == 0)
			snprintf(buf, sizeof buf, "unix://[anonymous socket]");
		else
			snprintf(buf, sizeof buf, "unix://%s", s_un->sun_path);
		break;
	}

	default:
		snprintf(buf, sizeof buf, "family:%d", sa->sa_family);
	}
	return xstrdup (buf);
}

static int 
is_numstr(const char *p)
{
	if (!*p)
		return 0;
	for (; *p && isascii(*p) && isdigit(*p);p++)
		;
	return *p == 0;
}

struct addrinfo *
net_address_parse(char const *addrstr, int server)
{
	struct addrinfo hints;
	struct addrinfo *res;
	int rc;
	char const *node;
	char const *service;
	char *nodebuf = NULL;

	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = server ? AI_PASSIVE : 0;
	hints.ai_family = AF_INET;

	service = strrchr(addrstr, ':');
	if (service) {
		size_t len = service - addrstr;

		if (len == 0)
			node = NULL;
		else {
			nodebuf = xmalloc(len + 1);
			memcpy(nodebuf, addrstr, len);
			nodebuf[len] = 0;
			node = nodebuf;
		}
		service++;
		if (!*service) {
			service = "1";
			hints.ai_flags |= AI_NUMERICSERV;
		}
	} else if (is_numstr(addrstr)) {
		node = NULL;
		service = addrstr;
		hints.ai_flags |= AI_NUMERICSERV;
	} else {
		node = addrstr;
		service = "1";
		hints.ai_flags |= AI_NUMERICSERV;		
	}
	
	rc = getaddrinfo(node, service, &hints, &res);
	free(nodebuf);
	switch (rc) {
	case 0:
		break;
	case EAI_SYSTEM:
		error(EX_USAGE, errno, "cannot parse address");
	case EAI_BADFLAGS:
	case EAI_SOCKTYPE:
		error(EX_OSERR, 0, "%s:%d: internal error converting address",
		      __FILE__,__LINE__);
	case EAI_MEMORY:
		error(1, ENOMEM, "exiting");
	default:
		error(EX_OSERR, 0, "getaddrinfo: %s", gai_strerror(rc));
	}
	return res;
}
