/* This file is part of rpipe
   Copyright (C) 2018-2021 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later
   <http://gnu.org/licenses/gpl.html>
   
   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <sysexits.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <syslog.h>
#include <limits.h>
#include "rpipe.h"

int verbose;

char const *progname;

void
setprogname(char *arg0)
{
	progname = strrchr(arg0, '/');
	if (progname)
		++progname;
	else
		progname = arg0;
}

void
verror_stderr(char const *fmt, int err, va_list ap)
{
	fprintf(stderr, "%s: ", progname);
	vfprintf(stderr, fmt, ap);
	if (err)
		fprintf(stderr, ": %s", strerror(err));
	fprintf(stderr, "\n");
}

void
verror_syslog(char const *fmt, int err, va_list ap)
{
	if (err) {
		static char fmtbuf[1024];
#define SUFFIX ": %m"
		size_t flen = strlen(fmt) + sizeof(SUFFIX);
		if (flen < sizeof(fmtbuf)) {
			int saved_errno = errno;
			strcat(strcpy(fmtbuf, fmt), SUFFIX);
			errno = err;
			vsyslog(LOG_ERR, fmtbuf, ap);
			errno = saved_errno;
		}
	} else
		vsyslog(LOG_ERR, fmt, ap);
}

void (*verror)(char const *fmt, int err, va_list ap) = verror_stderr;

void
error(int status, int err, char const *fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	verror(fmt, err, ap);
	va_end(ap);
	if (status)
		exit(status);
}

void
signal_setup(void (*handler)(int sig), int *sigv, int sigc)
{
	int i;
	struct sigaction act;

	act.sa_flags = 0;
	sigemptyset(&act.sa_mask);
	for (i = 0; i < sigc; i++)
		sigaddset(&act.sa_mask, sigv[i]);

	act.sa_handler = handler;
	for (i = 0; i < sigc; i++)
		sigaction(sigv[i], &act, NULL);
}

void
usage(void)
{
	printf("server: %s [OPTIONS] COMMAND [ARG...]\n", progname);
	printf("For each incoming TCPMUX connection invokes COMMAND with ARGs augmented\n");
	printf("with the arguments sent by the remote party in the first line and sends\n");
	printf("the rest of data to the COMMANDs stdin.\n\n");
	printf("client: %s [OPTIONS] ARG...\n", progname);
	printf("Pipes data over the network to the remote program's stdin.\n\n");
	printf("test mode: %s -T SERVER_ARGS -- CLIENT_ARGS\n", progname);
	printf("Starts server at arbitrary port on localhost and acts as a client\n");
	printf("\n");
	printf("OPTIONS are:\n\n");
	printf("  -a IPADDR[:PORT]    (Server) IP address and port to listen on.\n");
	printf("                      Default: 0.0.0.0:1\n");
	printf("                      (Client) IP address and port to connect to.\n");
	printf("  -g GROUP[,GROUP...] (Server only) Run with the given primary and\n");
	printf("                      supplementary group privileges.\n");
	printf("  -h                  Show this help screen and exit.\n");
	printf("  -n NUMBER           (Server only) Limit on the number of simultaneous\n");
	printf("                      connections.\n");
	printf("  -p FILE             (Server only) Read per-command user run-time\n");
	printf("                      privileges from FILE.\n");
	printf("  -S FACILITY         Log to the syslog FACILITY.\n");
	printf("  -s                  Enable server mode.\n");
	printf("  -T                  Enable test mode.\n");
	printf("  -u USER             (Server only) Run with that USER privileges\n");
	printf("  -V                  Print version number and exit.\n");
	printf("  -v                  Increase output verbosity.\n");
	printf("\n");
	printf("Send bug reports to <%s>.\n", PACKAGE_BUGREPORT);
#ifdef PACKAGE_URL
	printf("For more info, visit <%s>.\n", PACKAGE_URL);
#endif
}

void
version(void)
{
	printf("%s\n", PACKAGE_STRING);
	printf("\
Copyright (C) 2019-2021 Sergey Poznyakoff.\n\
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.\n\
This is free software: you are free to change and redistribute it.\n\
There is NO WARRANTY, to the extent permitted by law.\n");
}

static inline char const *
default_address(char const *address, char const *dfl)
{
	if (!address && (address = getenv("RPIPE_ADDRESS")) == NULL
	    && (address = dfl) == NULL)
		error(EX_USAGE, 0, "address not supplied");
	return address;
}

static struct {
	char const *name;
	int fac;
} syslog_facilities[] = {
	{ "auth", LOG_AUTH },
#ifdef LOG_AUTHPRIV
	{ "authpriv", LOG_AUTHPRIV },
#endif
	{ "cron", LOG_CRON },
	{ "daemon", LOG_DAEMON },
	{ "ftp", LOG_FTP },
	{ "kern", LOG_KERN },
	{ "lpr", LOG_LPR },
	{ "mail", LOG_MAIL },
	{ "news", LOG_NEWS },
	{ "user", LOG_USER },
	{ "uucp", LOG_UUCP },
	{ "local0", LOG_LOCAL0 },
	{ "local1", LOG_LOCAL1 },
	{ "local2", LOG_LOCAL2 },
	{ "local3", LOG_LOCAL3 },
	{ "local4", LOG_LOCAL4 },
	{ "local5", LOG_LOCAL5 },
	{ "local6", LOG_LOCAL6 },
	{ "local7", LOG_LOCAL7 },
	{ NULL }
};

static int
syslog_facility(char const *s)
{
	int i;
	char *p;
	unsigned long n;
	
	for (i = 0; syslog_facilities[i].name; i++) {
		if (strcasecmp(syslog_facilities[i].name, s) == 0)
			return syslog_facilities[i].fac;
	}

	errno = 0;
	n = strtoul(s, &p, 10);
	if (errno || *p)
		error(EX_USAGE, 0, "invalid syslog facility %s", s);
	if ((INT_MAX >> 3) < n)
		error(EX_USAGE, 0, "numeric facility out of allowed range: %s",
		      s);
	return n;
}


int
main(int argc, char **argv)
{
	int c;
	int pidmax = 128;
	char const *address = NULL;
	enum { RPIPE_CLIENT, RPIPE_SERVER, RPIPE_TEST } mode = RPIPE_CLIENT;
	char const *user = NULL;
	char const *group = NULL;
	
	setprogname(argv[0]);
	while ((c = getopt(argc, argv, "+a:g:hn:S:sp:Tu:Vv")) != EOF) {
		switch (c) {
		case 'a':
			address = optarg;
			break;
		case 'g':
			group = optarg;
			break;
		case 'h':
			usage();
			exit(0);
		case 'n':
			pidmax = atoi(optarg);
			break;
		case 'u':
			user = optarg;
			break;
		case 'p':
			privtab_read(optarg);
			break;
		case 'T':
			/* Self-test mode:
			     rpipe -T [SERVER-OPTS] -- [CLIENT-OPTS]
			 */
			mode = RPIPE_TEST;
			break;
		case 'S':
			openlog(progname, LOG_PID, syslog_facility(optarg));
			verror = verror_syslog;
			break;
		case 's':
			mode = RPIPE_SERVER;
			break;
		case 'V':
			version();
			exit(0);
		case 'v':
			verbose++;
			break;
		default:
			exit(EX_USAGE);
		}
	}

	argc -= optind;
	argv += optind;

	if (user || group)
		privtab_default(user, group);

	if (!argc)
		error(EX_USAGE, 0, "program to run not supplied");

	switch (mode) {
	case RPIPE_SERVER:
		rpipe_server(net_address_parse(default_address(address, ":1"),
					       1), pidmax, argc, argv);
		break;

	case RPIPE_CLIENT:
		rpipe_client(net_address_parse(default_address(address, NULL),
					       0), argc, argv);
		break;

	case RPIPE_TEST:
		rpipe_test(pidmax, argc, argv);
		break;
		
	default:
		abort();
	}
	
	return 0;
}

