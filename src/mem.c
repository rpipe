/* This file is part of rpipe
   Copyright (C) 2018 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>
   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <errno.h>
#include "rpipe.h"

void *
xmalloc(size_t s)
{
	void *p = malloc(s);
	if (!p)
		error(EX_OSERR, ENOMEM, "exiting");
	return p;
}

char *
xstrdup(char const *s)
{
	char *p = strdup(s);
	if (!p)
		error(EX_OSERR, ENOMEM, "exiting");
	return p;
}

void *
xcalloc(size_t nmemb, size_t size)
{
  void *p = calloc(nmemb, size);
  if (!p)
	  error(EX_OSERR, ENOMEM, "exiting");
  return p;
}

void *
xrealloc(void *p, size_t size)
{
       p = realloc(p, size);
       if (!p)
               error(EX_OSERR, ENOMEM, "exiting");
       return p;
}

void *
x2nrealloc(void *p, size_t *pn, size_t s)
{
        size_t n = *pn;

        if (!p) {
                if (!n) {
                        /* The approximate size to use for initial small
                           allocation requests, when the invoking code
                           specifies an old size of zero.  64 bytes is
                           the largest "small" request for the
                           GNU C library malloc.  */
                        enum { DEFAULT_MXFAST = 64 };

                        n = DEFAULT_MXFAST / s;
                        n += !n;
                }
        } else {
                /* Set N = ceil (1.5 * N) so that progress is made if N == 1.
                   Check for overflow, so that N * S stays in size_t range.
                   The check is slightly conservative, but an exact check isn't
                   worth the trouble.  */
                if ((size_t) -1 / 3 * 2 / s <= n)
                       error(EX_OSERR, ENOMEM, "exiting");
                n += (n + 1) / 2;
        }

        *pn = n;
        return xrealloc(p, n * s);
}
