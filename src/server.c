/* This file is part of rpipe
   Copyright (C) 2018-2021 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later
   <http://gnu.org/licenses/gpl.html>

   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
#include <sysexits.h>
#include <poll.h>
#include <ctype.h>
#include <signal.h>
#include "wordsplit.h"
#include "rpipe.h"
#include "iobuf.h"

typedef struct rpipe {
	size_t maxconn;       /* Max. number of connections */
	size_t nconn;         /* Actual number of connections served */
	size_t nfd;           /* Size of fds */
	struct pollfd *fds;   /* poll(2) data. */
	rpipe_server_t *srv;  /* Servers. */
	rpipe_server_t *avail;/* Link to the first available server */
	int argc;
	char **argv;
} rpipe_t;

enum {
	RET_SUCCESS = '+',
	RET_FAILURE = '-'
};

/* Servers */
typedef int (*server_handler_t)(rpipe_t *rp, rpipe_server_t *srv);
typedef void (*server_disconnect_t)(rpipe_t *rp, rpipe_server_t *srv);
typedef void (*server_free_t)(rpipe_server_t *srv);
typedef char *(*server_str_t)(rpipe_server_t *srv);

static int srv_listener_in(rpipe_t *rp, rpipe_server_t *srv);
static void srv_listener_free(rpipe_server_t *srv);
static char *srv_listener_str(rpipe_server_t *srv);

static int srv_init_in(rpipe_t *rp, rpipe_server_t *srv);
static int srv_start_session(rpipe_t *rp, rpipe_server_t *srv);
static int srv_net_in(rpipe_t *rp, rpipe_server_t *srv);
static int srv_net_out(rpipe_t *rp, rpipe_server_t *srv);
static void srv_net_disconnect(rpipe_t *rp, rpipe_server_t *srv);
static void srv_net_free(rpipe_server_t *srv);
static char *srv_net_str(rpipe_server_t *srv);
static void srv_net_format(struct rpipe_server_net *srv, int code, char const *fmt, ...);

static int srv_progin_out(rpipe_t *rp, rpipe_server_t *srv);
static void srv_progin_disconnect(rpipe_t *rp, rpipe_server_t *srv);
static void srv_progin_free(rpipe_server_t *srv);

static int srv_progout_in(rpipe_t *rp, rpipe_server_t *srv);
static int srv_progout_hup(rpipe_t *rp, rpipe_server_t *srv);
static void srv_progout_free(rpipe_server_t *srv);

static void progout_store_status(struct rpipe_server_progout *p, int status);
static void progout_transfer(struct rpipe_server_progout *p,
			     struct rpipe_server_net *net);

static struct srvdef {
	server_handler_t srv_in;
	server_handler_t srv_out;
	server_handler_t srv_hup;
	server_disconnect_t srv_dis;
	server_free_t srv_free;
	server_str_t srv_str;
} srvdef[] = {
	[rpipe_type_listener] = {
		.srv_in   = srv_listener_in,
		.srv_free = srv_listener_free,
		.srv_str  = srv_listener_str
	},
	[rpipe_type_init] = {
		.srv_in   = srv_init_in,
		.srv_free = srv_net_free,
		.srv_str  = srv_net_str,
		.srv_dis  = srv_net_disconnect
	},
	[rpipe_type_net] = {
		.srv_in   = srv_net_in,
		.srv_out  = srv_net_out,
		.srv_free = srv_net_free,
		.srv_str  = srv_net_str,
		.srv_dis  = srv_net_disconnect
	},
	[rpipe_type_progin] = {
		.srv_out  = srv_progin_out,
		.srv_free = srv_progin_free,
		.srv_dis  = srv_progin_disconnect
	},
	[rpipe_type_progout] = {
		.srv_in   = srv_progout_in,
		.srv_hup  = srv_progout_hup,
		.srv_free = srv_progout_free,
	}
};

/* Generalized server API */

/* Allocate new server for file descriptor FD and event set EVT */
rpipe_server_t *
rpipe_server_new(rpipe_t *rp, enum rpipe_server_type type, int fd, int evt)
{
	rpipe_server_t *srv = rp->avail;
	rp->avail = rp->avail->comm.next;
	memset(srv, 0, sizeof(*srv));
	srv->comm.type = type;
	srv->comm.next = NULL;
	srv->comm.pollfd = &rp->fds[srv - rp->srv];
	srv->comm.pollfd->fd = fd;
	srv->comm.pollfd->events = evt;
	return srv;
}

/* Return textual description of the server */
const char *
rpipe_server_str(rpipe_server_t *srv)
{
	if (!srv->comm.str) {
		if (srvdef[srv->comm.type].srv_str(srv))
			srv->comm.str = srvdef[srv->comm.type].srv_str(srv);
		else
			return "UNKNOWN";
	}
	return srv->comm.str;
}

/* Deallocate server resources */
void
rpipe_server_free(rpipe_t *rp, rpipe_server_t *srv)
{
	if (srv->comm.type == rpipe_type_available)
		return;
	srvdef[srv->comm.type].srv_free(srv);
	free(srv->comm.str);
	srv->comm.str = NULL;
	srv->comm.pollfd->fd = -1;
	srv->comm.type = rpipe_type_available;
	/* Return to the pool of available servers */
	srv->comm.next = rp->avail;
	rp->avail = srv;
}

void
rpipe_server_disconnect(rpipe_t *rp, rpipe_server_t *srv)
{
	if (srvdef[srv->comm.type].srv_dis)
		srvdef[srv->comm.type].srv_dis(rp, srv);
	close(srv->comm.pollfd->fd);
	srv->comm.pollfd->fd = -1;
	rpipe_server_free(rp, srv);
}

/* Server handler function */
int
rpipe_server_input(rpipe_t *rp, rpipe_server_t *srv)
{
	return srvdef[srv->comm.type].srv_in(rp, srv);
}

int
rpipe_server_output(rpipe_t *rp, rpipe_server_t *srv)
{
	return srvdef[srv->comm.type].srv_out(rp, srv);
}

int
rpipe_server_hangup(rpipe_t *rp, rpipe_server_t *srv)
{
	return srvdef[srv->comm.type].srv_hup
		? srvdef[srv->comm.type].srv_hup(rp, srv)
		: 0;
}

struct rpipe_server_progin *
rpipe_server_by_pid(rpipe_t *rp, pid_t pid)
{
	int i;

	for (i = 0; i < rp->nfd; i++) {
		if (rp->srv[i].comm.type == rpipe_type_progin
		    && rp->srv[i].progin.pid == pid)
			return &rp->srv[i].progin;
	}
	return NULL;
}

int
rpipe_listeners_enabled(rpipe_t *rp)
{
	return rp->fds[0].fd >= 0;
}

void
rpipe_toggle_listeners(rpipe_t *rp)
{
	int i;

	if (rpipe_listeners_enabled(rp))
		error(0, 0, "disabling listeners");
	else
		error(0, 0, "enabling listeners");

	for (i = 0; i < rp->nfd; i++)
		if (rp->srv[i].comm.type == rpipe_type_listener)
			rp->fds[i].fd = - rp->fds[i].fd;
}

ssize_t
iobuf_transfer(struct iobuf *dst, struct iobuf *src)
{
	ssize_t n = iobuf_data_size(src);
	if (n > 0) {
		ssize_t avail = iobuf_avail_size(dst);
		if (avail < n)
			n = avail;
		if (n) {
			memcpy(iobuf_avail_ptr(dst), iobuf_data_ptr(src), n);
			iobuf_avail_advance(dst, n);
			iobuf_data_advance(src, n);
		}
	}
	return 0;
}

/* **************************
 * Particular server types
 * ************************** */

/* Listener */

static int
srv_listener_in(rpipe_t *rp, rpipe_server_t *srv)
{
	struct sockaddr_storage sa;
	socklen_t salen = sizeof(sa);
	int fd;
	struct rpipe_server_listener *lsrv =
		(struct rpipe_server_listener*) srv;
	struct rpipe_server_net *isrv;

	fd = accept(lsrv->pollfd->fd, (struct sockaddr*) &sa, &salen);
	if (fd == -1) {
		error(0, errno, "accept");
		return 0; /* Don't signal error for listeners */
	}
	isrv = (struct rpipe_server_net *)
		rpipe_server_new(rp, rpipe_type_init, fd, POLLIN);
	if (!isrv) {
		error(0, 0, "too many open connections; pausing...");
		pause();
	}

	isrv->addr = xmalloc(salen);
	memcpy(isrv->addr, &sa, salen);
	isrv->addrlen = salen;
	if (verbose)
		error(0, 0,
		      "%s: connect from %s",
		      rpipe_server_str(srv),
		      rpipe_server_str((rpipe_server_t*)isrv));

	rp->nconn++;
	if (rp->nconn == rp->maxconn) {
		error(0, 0, "maximum number of simultaneous connections reached");
		rpipe_toggle_listeners(rp);
	}

	return 0;
}

//FIXME
static void
srv_listener_free(rpipe_server_t *srv)
{
	struct rpipe_server_listener *lsrv =
		(struct rpipe_server_listener*) srv;
	free(lsrv->addr);
}

static char *
srv_listener_str(rpipe_server_t *srv)
{
	struct rpipe_server_listener *lsrv =
		(struct rpipe_server_listener*) srv;
	return sockaddr_str(lsrv->addr, lsrv->addrlen);
}

/* Init server */
static int
srv_init_in(rpipe_t *rp, rpipe_server_t *srv)
{
	char *p;
	struct rpipe_server_net *isrv =
		(struct rpipe_server_net*) srv;
	ssize_t n = iobuf_fill(&isrv->ibuf, isrv->pollfd->fd);
	if (n == -1) {
		error(0, errno, "%s: read", rpipe_server_str(srv));
		return -1;
	}
	if (n == 0) {
		error(0, 0, "%s: unexpected EOF", rpipe_server_str(srv));
		return -1;
	}
	p = memchr(iobuf_data_ptr(&isrv->ibuf), '\n',
		   iobuf_data_size(&isrv->ibuf));
	if (p) {
		iobuf_data_advance(&isrv->ibuf, p - isrv->ibuf.buf + 1);
		*p = 0;
		if (p[-1] == '\r')
			*--p = 0;
		if (verbose)
			error(0, 0,
			      "%s: got '%s'", rpipe_server_str(srv),
			      isrv->ibuf.buf);
		return srv_start_session(rp, srv);
	}
	if (iobuf_avail_size(&isrv->ibuf) == 0) {
		error(0, 0, "%s: line too long", rpipe_server_str(srv));
		return -1;
	}
	return 0;
}

static char *
wordsplit_string(struct wordsplit *ws)
{
	size_t i;
	size_t n = ws->ws_offs + ws->ws_wordc;
	size_t len = n;
	char *result, *p, *q;

	if (n == 0)
		return xstrdup("");

	for (i = 0; i < n; i++) {
		len += strlen(ws->ws_wordv[i]);
	}
	result = xmalloc(len);
	p = result;
	for (i = 0; i < n; i++) {
		q = ws->ws_wordv[i];
		while (*q)
			*p++ = *q++;
		*p++ = ' ';
	}
	p[-1] = 0;
	return result;
}

/* Net I/O server */
static int
srv_start_session(rpipe_t *rp, rpipe_server_t *srv)
{
	struct rpipe_server_net *init =
		(struct rpipe_server_net*) srv;
	struct rpipe_server_net *net;
	struct rpipe_server_progin *progin;
	struct rpipe_server_progout *progout;
	int inpipe[2];
	int outpipe[2];
	pid_t pid;
	struct wordsplit ws;
	int i;
	char *command;

	ws.ws_offs = rp->argc;
	if (wordsplit(iobuf_data_ptr(&init->ibuf), &ws,
		      WRDSF_DOOFFS
		      | WRDSF_NOCMD
		      | WRDSF_NOVAR
		      | WRDSF_QUOTE))
		exit(EX_UNAVAILABLE);
	for (i = 0; i < rp->argc; i++)
		ws.ws_wordv[i] = rp->argv[i];

	command = wordsplit_string(&ws);
	
	if (verbose > 1) {
		error(0, 0, "%s: serving %s", rpipe_server_str(srv), command);
	}

	if (pipe(inpipe)) {
		error(0, errno, "%s: pipe", rpipe_server_str(srv));
		wordsplit_free(&ws);
		return -1;
	}
	if (pipe(outpipe)) {
		error(0, errno, "%s: pipe", rpipe_server_str(srv));
		wordsplit_free(&ws);
		close(inpipe[0]);
		close(inpipe[1]);
		return -1;
	}

	pid = fork();
	if (pid == 0) {
		if (dup2(inpipe[0], 0) == -1)
			error(EX_OSERR, errno, "dup2");
		if (dup2(outpipe[1], 1) == -1)
			error(EX_OSERR, errno, "dup2");

		for (i = 3; i < sysconf(_SC_OPEN_MAX); i++)
			close(i);
		runas(privtab_find(ws.ws_wordv[ws.ws_offs]));
		execv(ws.ws_wordv[0], ws.ws_wordv);
		error(EX_UNAVAILABLE, errno, "execv");
	}

	wordsplit_free(&ws);
	close(inpipe[0]);
	close(outpipe[1]);

	if (pid == -1) {
		error(0, errno, "%s: fork", rpipe_server_str(srv));
		close(inpipe[1]);
		close(outpipe[0]);
		free(command);
		return -1;
	}

	/* Master */

	/* Change type of the server. */
	net = (struct rpipe_server_net *)srv;
	net->type = rpipe_type_net;

	/* Create new output server */
	progin = (struct rpipe_server_progin *)
		  rpipe_server_new(rp, rpipe_type_progin, inpipe[1], POLLOUT);
# define PREFIX "stdin of "
	progin->str = xmalloc(strlen(command) + sizeof PREFIX);
	strcat(strcpy(progin->str, PREFIX), command);
# undef PREFIX

	progin->pid = pid;
	progin->state = prog_startup;

	/* Link the two servers together */
	progin->net = net;
	net->progin = progin;

	/* Create the progout server */
	progout = (struct rpipe_server_progout *)
		   rpipe_server_new(rp, rpipe_type_progout, outpipe[0], POLLIN);
# define PREFIX "stdout of "
	progout->str = xmalloc(strlen(command) + sizeof PREFIX);
	strcat(strcpy(progout->str, PREFIX), command);
# undef PREFIX

	/* Link it to the prog */
	progout->progin = progin;
	progin->out = progout;

	/* Link it to the net */
	progout->net = net;
	net->progout = progout;
	
	/* Temporarily disable event handling for the net server. It will be
	   re-enabled when first POLLOUT event is detected on the prog end
	   or when the pid is reported to terminate, whichever first.
	 */
	net->pollfd->events = 0;

	return 0;
}

static void srv_progin_shutdown(struct rpipe_server_progin *);

static int
srv_net_in(rpipe_t *rp, rpipe_server_t *srv)
{
	struct rpipe_server_net *net =
		(struct rpipe_server_net *)srv;

	if (iobuf_avail_size(&net->ibuf)) {
		ssize_t n = iobuf_fill(&net->ibuf, net->pollfd->fd);
		if (n == -1) {
			error(0, errno, "%s: read", rpipe_server_str(srv));
			srv_net_format(net, RET_FAILURE, "%s", "I/O error");
			net->pollfd->events &= ~POLLIN;
			return -1;
		} else if (n == 0) {
			if (verbose)
				error(0, 0, "%s: EOF", rpipe_server_str(srv));
			/* No more input is expected */
			net->pollfd->events &= ~POLLIN;
			if (!iobuf_data_size(&net->ibuf))
			    srv_progin_shutdown(net->progin);
		} else if (net->progin) /* n > 0 */
			net->progin->pollfd->events |= POLLOUT; /* PROG_POLLOUT */
	} /* FIXME: else timeout? */

	return 0;
}

static int
srv_net_out(rpipe_t *rp, rpipe_server_t *srv)
{
	struct rpipe_server_net *net =
		(struct rpipe_server_net *)srv;
	ssize_t n;
	
	progout_transfer(net->progout, net);

	if (iobuf_data_size(&net->obuf) == 0) {
		if (!net->progin && net->progout->status_index == 0)
			rpipe_server_disconnect(rp, srv);
		net->pollfd->events &= ~POLLOUT; /* NET_POLLOUT */
		return 0;
	}

	n = iobuf_flush(&net->obuf, net->pollfd->fd);
	if (n == -1) {
		error(0, errno, "%s: write", rpipe_server_str(srv));
		return -1;
	}
	if (n == 0) {
		// FIXME: eof
		return -1;
	}

	return 0;
}

static void
srv_net_disconnect(rpipe_t *rp, rpipe_server_t *srv)
{
	struct rpipe_server_net *net =
		(struct rpipe_server_net *)srv;
	if (net->progin) {
		rpipe_server_disconnect(rp, (rpipe_server_t*) net->progin);
	}
	if (net->progout) {
		rpipe_server_disconnect(rp, (rpipe_server_t*) net->progout);
	}

	rp->nconn--;
	if (!rpipe_listeners_enabled(rp))
		rpipe_toggle_listeners(rp);
}

static int
srv_net_shutdown(struct rpipe_server_net *net, int what)
{
	int rc = shutdown(net->pollfd->fd, what);
	if (rc == 0) {
		switch (what) {
		case SHUT_WR:
			net->progout = NULL;
			break;

		case SHUT_RD:
			net->progin = NULL;
			break;

		case SHUT_RDWR:
			net->progin = NULL;
			net->progout = NULL;
		}
	}
	return rc;
}

static void
srv_net_free(rpipe_server_t *srv)
{
	struct rpipe_server_net *net = (struct rpipe_server_net*) srv;
	free(net->addr);
}

static char *
srv_net_str(rpipe_server_t *srv)
{
	struct rpipe_server_net *net = (struct rpipe_server_net*) srv;
	return sockaddr_str(net->addr, net->addrlen);
}

static void
srv_net_format(struct rpipe_server_net *net, int code, char const *fmt, ...)
{
	int i = 0;
	struct iobuf *bp = &net->obuf;

	if (!net->progin)
		return;
	iobuf_reset(bp);
	bp->buf[i++] = code;
	if (fmt) {
		va_list ap;
		ssize_t n;

		va_start(ap, fmt);
		bp->buf[i++] = ' ';
		n = vsnprintf(bp->buf + i, sizeof(bp->buf) - i - 2, fmt, ap);
		i += n;
		if (i > sizeof(net->obuf.buf) - 2)
			i = sizeof(net->obuf.buf) - 2;
	}
	memcpy (bp->buf + i, "\r\n", 2);
	bp->avail = i + 2;
	bp->start = 0;
	net->pollfd->events |= POLLOUT;
}


/*
 * Program input server: sends data obtained from the network to the stdin
 * of the program.
 */

static void
srv_progin_shutdown(struct rpipe_server_progin *prog)
{
	/* Close and reset the pipe descriptor. */
	close(prog->pollfd->fd);
	prog->pollfd->fd = -1;
	/* Change server state */
	prog->state = prog_terminating;
}

static int
srv_progin_out(rpipe_t *rp, rpipe_server_t *srv)
{
	struct rpipe_server_progin *prog = (struct rpipe_server_progin *)srv;
	ssize_t n;

	if (prog->state == prog_startup) {
		srv_net_format(prog->net, RET_SUCCESS, "command started");
		prog->net->pollfd->events |= POLLIN | POLLOUT;
		prog->state = prog_running;
	}

	iobuf_transfer(&prog->buf, &prog->net->ibuf);

	if (iobuf_data_size(&prog->buf) == 0) {
		if (!(prog->net->pollfd->events & POLLIN)) {
			/* The corresponding net socket has been shut down
			   for writing. There is no more data available. */
			srv_progin_shutdown(prog);
		}
		prog->pollfd->events &= ~POLLOUT; /* PROG_POLLOUT */
		return 0;
	}

	n = iobuf_flush(&prog->buf, prog->pollfd->fd);
	if (n <= 0) {
		if (n == -1)
			error(0, errno, "%s: write", rpipe_server_str(srv));
		else
			error(0, 0, "%s: can't write", rpipe_server_str(srv));
		/* Notify the net server */
		srv_net_format(prog->net, RET_FAILURE, "%s", "I/O error");
		/* No more input is expected */
		prog->net->pollfd->events &= ~POLLIN;
		srv_progin_shutdown(prog);
	}
	return 0;
}

static void
srv_progin_disconnect(rpipe_t *rp, rpipe_server_t *srv)
{
	struct rpipe_server_progin *prog = (struct rpipe_server_progin *)srv;
	kill(prog->pid, SIGKILL);
}

static void
srv_progin_free(rpipe_server_t *srv)
{
	if (srv->progin.net)
		srv->progin.net->progin = NULL;
	if (srv->progin.out)
		srv->progin.out->progin = NULL;
}

/*
 * Program output server: reads data from the program's stdout
 */

static int
srv_progout_in(rpipe_t *rp, rpipe_server_t *srv)
{
	struct rpipe_server_progout *progout = &srv->progout;
	if (iobuf_avail_size(&progout->buf)) {
		ssize_t n = iobuf_fill(&progout->buf, progout->pollfd->fd);
		if (n == -1) {
			error(0, errno, "%s: read", rpipe_server_str(srv));
			srv_net_format(progout->net, RET_FAILURE,
				       "%s", "I/O error");
			progout->net->pollfd->events &= ~POLLIN;
			if (progout->progin)
				srv_progin_shutdown(progout->progin);
			return -1;
		} else if (n == 0) {
			if (verbose)
				error(0, 0, "%s: EOF", rpipe_server_str(srv));
			progout->pollfd->events &= ~POLLIN;
		} else /* n > 0 */
		        progout->net->pollfd->events |= POLLOUT; /* NET_POLLOUT */
	} /* FIXME: else timeout? */

	return 0;
}

static int
srv_progout_hup(rpipe_t *rp, rpipe_server_t *srv)
{
	struct rpipe_server_progout *progout = &srv->progout;
	if (progout->pollfd->revents == POLLHUP && (progout->pollfd->events & POLLIN)) {
		if (verbose)
			error(0, 0, "%s: HUP", rpipe_server_str(srv));
		progout->pollfd->events &= ~POLLIN;
	}
	return 0;
}

static void
srv_progout_free(rpipe_server_t *srv)
{
	if (srv->progout.progin)
		srv->progout.progin->out = NULL;
	if (srv->progout.net)
		srv_net_shutdown(srv->progout.net, SHUT_WR);
}

static inline void
progout_crlf(struct rpipe_server_progout *p)
{
	p->status_buffer[p->status_index++] = '\n';
	p->status_buffer[p->status_index++] = '\r';
}

static void
progout_store_status(struct rpipe_server_progout *p, int status)
{
	static char dig[] = "0123456789";
	p->status_index = 0;
	progout_crlf(p);
	if (status == 0)
		p->status_buffer[p->status_index++] = dig[status];
	else {
		while (status) {
			p->status_buffer[p->status_index++] = dig[status % 10];
			status /= 10;
		}
	}
	progout_crlf(p);
	p->status_buffer[p->status_index++] = '.';
	if (p->encode_state == crlfdot_encode_char)
		progout_crlf(p);
	p->net->pollfd->events |= POLLOUT; /* NET_POLLOUT */
}

static void
progout_transfer(struct rpipe_server_progout *p, struct rpipe_server_net *net)
{
	if (iobuf_data_size(&p->buf))
		crlfdot_encoder(&p->encode_state, &p->buf, &net->obuf);
	else if (!p->progin) {
		while (iobuf_avail_size(&net->obuf) && p->status_index > 0) {
			if (iobuf_putc(&net->obuf,
				       p->status_buffer[p->status_index-1])
			    == -1)
				break;
			p->status_index--;
		}
	}
}

static rpipe_t *
rpipe_alloc(int nf, int np, int argc, char **argv)
{
	rpipe_t *rp;
	int i;
	
	rp = xcalloc(1, sizeof(*rp));
	rp->maxconn = np;
	rp->nfd = nf + 2*np;
	rp->fds = xcalloc(rp->nfd, sizeof(rp->fds[0]));
	rp->srv = xcalloc(rp->nfd, sizeof(rp->srv[0]));
	for (i = 0; i < rp->nfd; i++) {
		rp->fds[i].fd = -1;
		rp->srv[i].comm.next = &rp->srv[i + 1];
	}
	rp->srv[i-1].comm.next = NULL;
	rp->avail = rp->srv;
	rp->argc = argc;
	rp->argv = argv;
	return rp;
}

static rpipe_t *
net_setup(struct addrinfo *res, int np, int argc, char **argv)
{
	rpipe_t *rp;
	struct addrinfo *ap;
	size_t nf;
	int i;

	nf = 0;
	for (ap = res; ap; ap = ap->ai_next)
		nf++;

	rp = rpipe_alloc(nf, np, argc, argv);

	for (i = 0, ap = res; ap; i++, ap = ap->ai_next) {
		int fd;
		int t = 1;
		struct rpipe_server_listener *srv;

		fd = socket(ap->ai_addr->sa_family, SOCK_STREAM, 0);
		if (fd == -1) {
			error(EX_OSERR, errno, "can't create socket");
		}
		srv = (struct rpipe_server_listener *)
			rpipe_server_new(rp, rpipe_type_listener, fd, POLLIN);

		srv->addr = xmalloc(ap->ai_addrlen);
		memcpy(srv->addr, ap->ai_addr, ap->ai_addrlen);
		srv->addrlen = ap->ai_addrlen;

		setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &t, sizeof(t));
		if (bind(fd, srv->addr, srv->addrlen) == -1) {
			error(EX_OSERR, errno, "bind %s",
			      rpipe_server_str((rpipe_server_t*)srv));
		}
		if (listen(fd, 8) == -1) {
			error(EX_OSERR, errno, "listen %s",
			      rpipe_server_str((rpipe_server_t*)srv));
		}
	}
	return rp;
}

void
report_prog_status(int status, char *prog)
{
	if (WIFEXITED(status)) {
		if (verbose) {
			int s = WEXITSTATUS(status);
			if (s == 0)
				error(0, 0, "%s exited successfully", prog);
			else
				error(0, 0, "%s exited with status %d", prog, s);
		}
	} else if (WIFSIGNALED(status)) {
		error(0, 0,
		      "%s terminated on signal %d%s",
		      prog,
		      WTERMSIG(status),
		      WCOREDUMP(status) ? " (core dumped)" : "");
	} else if (WIFSTOPPED(status)) {
		error(0, 0, "%s stopped", prog);
	} else {
		error(0, 0,
		      "%s terminated with unrecognized status: %d",
		      prog, status);
	}
}

static void
report_pid_status(int status, uid_t pid)
{
	char sbuf[80];
	snprintf(sbuf, sizeof(sbuf), "%lu", (unsigned long)pid);
	report_prog_status(status, sbuf);
}

static void
proc_cleanup(rpipe_t *rp)
{
	pid_t pid;
	int status;
	while ((pid = waitpid(-1, &status, WNOHANG)) > 0) {
		struct rpipe_server_progin *prog = rpipe_server_by_pid(rp, pid);
		if (!prog) {
			error(0, 0, "unknown pid %lu terminated (status %d)",
			      (unsigned long) pid, status);
			continue;
		}
		progout_store_status(prog->out, status);
		report_pid_status(status, pid);
		rpipe_server_free(rp, (rpipe_server_t*)prog);
	}
}

static volatile int stopsig;
static volatile int cleanup;

static void
server_sig_handler(int sig)
{
	switch (sig) {
	case SIGCHLD:
		cleanup++;
		break;

	case SIGPIPE:
		/* Ignore */
		break;

	default:
		stopsig = 1;
		break;
	}
}

void
rpipe_server_run(rpipe_t *rp)
{
	int i;
	static int sigv[] = {
		SIGCHLD,
		SIGTERM,
		SIGQUIT,
		SIGINT,
		SIGHUP,
		SIGUSR1,
		SIGALRM,
		SIGPIPE
	};
	static int sigc = sizeof(sigv)/sizeof(sigv[0]);
	
	signal_setup(server_sig_handler, sigv, sigc);

	while (1) {
		int n;

		if (cleanup) {
			proc_cleanup(rp);
			cleanup--;
		}
		if (stopsig)
			break;

		n = poll(rp->fds, rp->nfd, -1);
		if (n == -1) {
			if (errno != EINTR)
				error(0, errno, "poll");
			continue;
		}

		for (i = 0; n > 0 && i < rp->nfd; i++) {
			if (rp->fds[i].fd < 0)
				continue;
			if (rp->fds[i].revents)
				n--;
			if ((rp->fds[i].events|POLLHUP) & rp->fds[i].revents) {
				int rc = 0;
				rpipe_server_t *srv = &rp->srv[i];
				
				if (rp->fds[i].revents & POLLIN)
					rc |= rpipe_server_input(rp, srv);
				if (rp->fds[i].revents & POLLOUT)
					rc |= rpipe_server_output(rp, srv);
				if (rp->fds[i].revents & POLLHUP)
					rc |= rpipe_server_hangup(rp, srv);
				if (rc) {
					//FIXME
					rpipe_server_disconnect(rp, srv);
				}
			}
		}
	}
}

void
rpipe_server(struct addrinfo *ap, int np, int argc, char **argv)
{
	return rpipe_server_run(net_setup(ap, np, argc, argv));
}

static pid_t server_pid;

static void
stop_server(void)
{
	kill(server_pid, SIGTERM);
}

static void
client_sig_handler(int sig)
{
	stop_server();
}

void
rpipe_test(int np, int argc, char **argv)
{
	int i;
	int c_argc = 0;
	char **c_argv = NULL;
	struct rpipe_server_listener *srv;
	struct addrinfo *ap, hints;
	int fd;
	rpipe_t *rp;
	socklen_t len;
	static int sigv[] = {
		SIGTERM,
		SIGQUIT,
		SIGINT,
		SIGHUP,
		SIGPIPE
	};
	static int sigc = sizeof(sigv)/sizeof(sigv[0]);
	
	/* Split arguments into server and client parts */
	for (i = 0; i < argc; i++) {
		if (strcmp(argv[i], "--") == 0) {
			c_argc = argc - i - 1;
			c_argv = argv + i + 1;
			argv[i] = NULL;
			argc = i;
			break;
		}
	}
	if (c_argv == NULL)
		error(EX_USAGE, 0, "test mode usage: rpipe SERVER_ARGS -- CLIENT_ARGS");
	
	/* Find first free port */
	memset(&hints, 0, sizeof(hints));
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = 0;
	hints.ai_family = AF_INET;

	if (getaddrinfo("127.0.0.1", NULL, &hints, &ap))
		error(EX_OSERR, errno, "getaddrinfo: %s", gai_strerror(i));
	
	fd = socket(PF_INET, SOCK_STREAM, 0);
	if (fd < 0)
		error(EX_OSERR, errno, "socket");
	i = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &i, sizeof(i));

	((struct sockaddr_in*)ap->ai_addr)->sin_port = 0;
	if (bind(fd, ap->ai_addr, ap->ai_addrlen) < 0)
		error(EX_OSERR, errno, "bind");

	if (listen(fd, 8) == -1)
		error(EX_OSERR, errno, "listen");

	len = ap->ai_addrlen;
	if (getsockname(fd, ap->ai_addr, &len))
		error(EX_OSERR, errno, "getsockname");
	
        /* Initialize the rpipe_t structure for the server */
	rp = rpipe_alloc(1, np, argc, argv);
	srv = (struct rpipe_server_listener *)
		rpipe_server_new(rp, rpipe_type_listener, fd, POLLIN);
	srv->addr = xmalloc(ap->ai_addrlen);
	memcpy(srv->addr, ap->ai_addr, ap->ai_addrlen);
	srv->addrlen = ap->ai_addrlen;

	/* Fork the server */
	server_pid = fork();
	if (server_pid == -1)
		error(EX_OSERR, errno, "fork");

	if (server_pid == 0) {
		/* Server */
		rpipe_server_run(rp);
		exit(0);
	}
	
	/* Client */
	close(fd);
	atexit(stop_server);
	signal_setup(client_sig_handler, sigv, sigc);
	rpipe_client_run(ap, c_argc, c_argv);
}
