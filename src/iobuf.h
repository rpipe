/* This file is part of rpipe
   Copyright (C) 2018-2021 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later
   <http://gnu.org/licenses/gpl.html>
   
   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/

static inline void
iobuf_reset(struct iobuf *bp)
{
	bp->start = bp->avail = 0;
}

/* Return the count of data bytes in the buffer */
static inline size_t
iobuf_data_size(struct iobuf *bp)
{
	return bp->avail - bp->start;
}

static inline char *
iobuf_data_ptr(struct iobuf *bp)
{
	return bp->buf + bp->start;
}

static inline void
iobuf_data_advance(struct iobuf *bp, size_t n)
{
	bp->start += n;
	if (bp->start == bp->avail)
		iobuf_reset(bp);
}

static inline int
iobuf_putback(struct iobuf *bp)
{
	return bp->buf[--bp->start-1];
}

static inline int
iobuf_getc(struct iobuf *bp)
{
	int c = -1;
	if (iobuf_data_size(bp) > 0) {
		c = *iobuf_data_ptr(bp);
		iobuf_data_advance(bp, 1);
	}
	return c;
}

/* Return the count of available bytes in the buffer */
static inline size_t
iobuf_avail_size(struct iobuf *bp)
{
	return sizeof(bp->buf) - bp->avail;
}

static inline char *
iobuf_avail_ptr(struct iobuf *bp)
{
	return bp->buf + bp->avail;
}

static inline void
iobuf_avail_advance(struct iobuf *bp, size_t n)
{
	bp->avail += n;
	if (iobuf_avail_size(bp) == 0 && iobuf_data_size(bp) == 0)
		iobuf_reset(bp);
}

static inline int
iobuf_putc(struct iobuf *bp, int c)
{
	if (iobuf_avail_size(bp) > 0) {
		*iobuf_avail_ptr(bp) = c;
		iobuf_avail_advance(bp, 1);
		return c;
	}
	return -1;
}

static inline ssize_t
iobuf_fill(struct iobuf *bp, int fd)
{
	ssize_t n = read(fd, iobuf_avail_ptr(bp), iobuf_avail_size(bp));
	if (n >= 0)
		iobuf_avail_advance(bp, n);
	return n;
}

static inline ssize_t
iobuf_flush(struct iobuf *bp, int fd)
{
	ssize_t n = write(fd, iobuf_data_ptr(bp), iobuf_data_size(bp));
	if (n >= 0)
		iobuf_data_advance(bp, n);
	return n;
}

ssize_t iobuf_transfer(struct iobuf *dst, struct iobuf *src);
