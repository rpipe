/* This file is part of rpipe
   Copyright (C) 2018-2021 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later
   <http://gnu.org/licenses/gpl.html>
   
   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/
#include <config.h>
#include <stdlib.h>
#include <unistd.h>
#include "rpipe.h"
#include "iobuf.h"

static enum crlfdot_encoder_state
new_encoder_state(int c)
{
	switch (c) {
	case '\n':
		return crlfdot_encode_lf;
	}
	return crlfdot_encode_char;
}

/* Move min(isize,osize) bytes from iptr to optr, byte-stuffing each
   '.' appearing at the beginning of a line */
void
crlfdot_encoder(enum crlfdot_encoder_state *state,
		struct iobuf *ibuf, struct iobuf *obuf)
{
	while (iobuf_avail_size(obuf) && iobuf_data_size(ibuf)) {
		unsigned char c = iobuf_getc(ibuf);

		if (c == '\n') {
			if (iobuf_avail_size(obuf) < 2)
				break;
			else
				iobuf_putc(obuf, '\r');
		} else if (c == '.'
			   && (*state == crlfdot_encode_init ||
			       *state == crlfdot_encode_lf)) {
			if (iobuf_avail_size(obuf) < 2) {
				break;
			}
			iobuf_putc(obuf, '.');
		}
		iobuf_putc(obuf, c);
		*state = new_encoder_state (c);
	}
}

/* Table-driven CRLFdot decoder. */
enum symbol {
	CR,
	LF,
	DOT,
	CHR
};

static char abc[] = "\r\n.";

static inline int
symbol_of(int c)
{
	int i;
	for (i = 0; abc[i] && abc[i] != c; i++)
		;
	return i;
}

enum {
	NSYM = 4,
	NSTATES = 10
};


static int transition[NSTATES][NSYM] = {
	{   2,   1,   4,   1 },
	{   2,   1,   1,   1 },
	{   6,   3,   6,   6 },
	{   2,   0,   4,   0 },
	{   5,   6,   1,   6 },
	{   7,   8,   7,   7 },
	{   1,   1,   1,   1 },
	{   6,   6,   6,   6 },
	{  -1,  -1,  -1,  -1 }
};

static int wantinput[NSTATES] = { 1, 1, 1, 1, 1, 1, 0, 0, 0 };

enum {
	NIL = -1,
	INP = -2
};

static int output[NSTATES][NSYM] = {
	{ NIL, INP, NIL, INP },
	{ NIL, INP, INP, INP },
	{  CR, INP,  CR,  CR },
	{ NIL, INP, NIL, INP },
	{ NIL, DOT, INP, DOT },
	{ DOT, NIL, INP, DOT },
	{ INP, INP, INP, INP },
	{  CR,  CR,  CR,  CR },
	{ NIL, NIL, NIL, NIL },
};

void
crlfdot_decoder(struct crlfdot_decoder_state *st, struct iobuf *obuf,
		struct iobuf *ibuf)
{
	while (st->state != -1
	       && iobuf_avail_size(obuf) && iobuf_data_size(ibuf)) {
		int sym, c;
		
		if (wantinput[st->state]) {
			c = iobuf_getc(ibuf);
			if (c == -1) {
				iobuf_putback(ibuf);
				break;
			}
			st->input = c;
		}
		sym = symbol_of(st->input);	

		c = output[st->state][sym];

		switch (c) {
		case NIL:
			break;
		case INP:
			iobuf_putc(obuf, st->input);
			break;
		default:
			iobuf_putc(obuf, abc[c]);
		}

		st->state = transition[st->state][sym];
	}
}
