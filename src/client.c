/* This file is part of rpipe
   Copyright (C) 2018-2021 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later
   <http://gnu.org/licenses/gpl.html>

   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/
#include <config.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <signal.h>
#include <netdb.h>
#include <stdio.h>
#include <errno.h>
#include <sysexits.h>
#include <unistd.h>
#include <poll.h>
#include <limits.h>
#include "rpipe.h"
#include "iobuf.h"

void
client_sig_handler(int sig)
{
	error(EX_OSERR, 0, "got signal %s", strsignal(sig));
}

static int client_sigv[] = {
	SIGCHLD,
	SIGTERM,
	SIGQUIT,
	SIGINT,
	SIGHUP,
	SIGUSR1,
	SIGALRM,
	SIGPIPE
};
static int client_sigc = sizeof(client_sigv)/sizeof(client_sigv[0]);

static int
valid_pollfd(struct pollfd *pf, int n)
{
	while (n--) {
		if (pf->fd >= 0 && pf->events)
			return 1;
		++pf;
	}
	return 0;
}

enum {
	NETIN,
	NETOUT,
	STDIN,
	STDOUT,
	NFD
};

static void
waitreply(int fd)
{
	struct pollfd fds[NFD];
	struct iobuf buf[NFD];
	struct crlfdot_decoder_state state = CRLFDOT_DECODER_STATE_INIT;
	
	memset(&fds, 0, sizeof(fds));

	fds[NETIN].fd = fd;
	fds[NETIN].events = POLLIN;

	fds[NETOUT].fd = fd;
	fds[NETOUT].events = 0;
	
	fds[STDIN].fd = 0;
	fds[STDIN].events = POLLIN;

	fds[STDOUT].fd = 1;
	fds[STDOUT].events = 0;

	memset(&buf, 0, sizeof(buf));

	while (valid_pollfd(fds, NFD)) {
		/*
		 * Save the currently requested STDIN events.  They could
		 * be modified prior to their actual use.
		 */
		int stdin_evt = fds[STDIN].events;
		
		ssize_t n = poll(fds, NFD, -1);
		if (n == -1) {
			if (errno != EINTR)
				error(0, errno, "poll");
			continue;
		}

		/* Check if there is input from the net */
		if (fds[NETIN].revents & POLLIN) {
			n = iobuf_fill(&buf[NETIN], fds[NETIN].fd);
			if (n == -1) {
				error(EX_OSERR, errno, "net read");
			} else if (n == 0) {
				shutdown(fds[NETIN].fd, SHUT_RD);
				fds[NETIN].fd = -fds[NETIN].fd;
			} else {
				fds[STDOUT].events |= POLLOUT;
				if (iobuf_avail_size(&buf[NETIN]) == 0)
					fds[NETIN].events &= ~POLLIN;
			}
		}

		/*
		 * Check whether the network can be written to.  This is
		 * where the requested STDIN events can be modified (see
		 * the comment before stdin_evt above.
		 */
		if (fds[NETOUT].revents & POLLOUT) {
			iobuf_transfer(&buf[NETOUT], &buf[STDIN]);
			if (iobuf_avail_size(&buf[STDIN]))
				fds[STDIN].events |= POLLIN;

			n = iobuf_flush(&buf[NETOUT], fds[NETOUT].fd);
			if (n == -1) {
				error(EX_OSERR, errno, "net write");
			} else if (n == 0) {
				error(EX_OSERR, ENOSPC, "net write");
			}
			if (iobuf_data_size(&buf[NETOUT]) == 0) {
				fds[NETOUT].events &= ~POLLOUT;
			}
		}

		/*
		 * Check whether there are some data waiting on the input,
		 * or the input is exhausted.  The latter is tricky because
		 * both POLLIN and POLLHUP can be set simultaneously.  On
		 * the other hand, if POLLHUP alone is set, the input is
		 * actually exhausted only if the POLLIN bit was requested
		 * when polling.  Its actual state in the fds[STDIN].events
		 * field can't be relied upon, because it could have been
		 * changed when analyzing NETOUT state above.  Hence the need
		 * for the additional variable stdin_evt.
		 */
		if (fds[STDIN].revents & POLLIN) {
			n = iobuf_fill(&buf[STDIN], fds[STDIN].fd);
			if (n == -1) {
				error(EX_OSERR, errno, "stdin read");
			} else if (n == 0) {
				shutdown(fds[NETOUT].fd, SHUT_WR);
				fds[NETOUT].fd = -fds[NETOUT].fd;
				fds[STDIN].events = 0;
			} else {
				fds[NETOUT].events |= POLLOUT;
				if (iobuf_avail_size(&buf[STDIN]) == 0)
					fds[STDIN].events &= ~POLLIN;
			}
		} else if (fds[STDIN].revents == POLLHUP && (stdin_evt & POLLIN)) {
			/* This indicates EOF on input */
			fds[STDIN].fd = -1;
		}		

		/*
		 * Shutdown the write end of the net socket, if EOF was seen
		 * on input.  This will indicate EOF for the remote party.
		 * However, if at least one of NETOUT or STDIN buffers contains
		 * some data, it would be premature to do so.  In this case
		 * wait until the data have been transferred to the remote end.
		 */ 
		if (fds[STDIN].fd == -1 && fds[NETOUT].fd >= 0 &&
		    iobuf_data_size(&buf[NETOUT]) == 0 &&
		    iobuf_data_size(&buf[STDIN]) == 0) {
			shutdown(fds[NETOUT].fd, SHUT_WR);
			fds[NETOUT].fd = -fds[NETOUT].fd;
		}

		/*
		 * Output the data obtained from the network.  Shut down
		 * both ends of the network socket if we have received the
		 * CR LF '.' CR LF marker.  This indicates end of the loop.
		 */
		if (fds[STDOUT].revents & POLLOUT) {
			crlfdot_decoder(&state, &buf[STDOUT], &buf[NETIN]);
			if (iobuf_avail_size(&buf[NETIN]))
				fds[NETIN].events |= POLLIN;
			if (iobuf_data_size(&buf[STDOUT]) == 0)
				fds[STDOUT].events &= ~POLLOUT;
			else {
				n = iobuf_flush(&buf[STDOUT], fds[STDOUT].fd);
				if (n == -1) {
					error(EX_OSERR, errno, "stdout write");
				} else if (n == 0) {
					error(EX_OSERR, ENOSPC, "stdout write");
				}
			}
			if (state.state == -1) {
				/* Final state reached. */
				fds[STDIN].events = 0;
				if (fds[NETOUT].fd >= 0) {
					shutdown(fds[NETOUT].fd, SHUT_WR);
					fds[NETOUT].fd = -fds[NETOUT].fd;
				}
				if (fds[NETIN].fd >= 0) {
					shutdown(fds[NETIN].fd, SHUT_RD);
					fds[NETIN].fd = -fds[NETIN].fd;
				}
			}
		}
	}

	close(fd);

	/* Read in and analyze exit status of the remote program. */
	if (state.state == -1 && iobuf_data_size(&buf[NETIN])) {
		char *p;
		unsigned long u = 0;
		static char dig[] = "0123456789";
		int c;
		int status;
		int ok = 0;
		while ((c = iobuf_getc(&buf[NETIN])) != -1
		       && (p = strchr(dig, c)) != NULL) {
			u = u * 10 + p - dig;
			ok = 1;
		}
		if (!(c == '\r' && iobuf_getc(&buf[NETIN]) == '\n'))
			error(0, 0, "remote protocol error: CRLF missing");
		if (!ok || u > INT_MAX)
			error(EX_UNAVAILABLE, 0,
			      "remote protocol error: exit status out of range");
		status = u;
		if (verbose)
			report_prog_status(status, "remote command");
		if (WIFEXITED(status))
			exit(WEXITSTATUS(status));
	}
	exit(EX_OSERR);
}

static void
trim(char *buf)
{
	size_t len = strlen(buf);
	if (len > 0 && buf[len-1] == '\n') {
		--len;
		if (len > 0 && buf[len-1] == '\r')
			--len;
		buf[len] = 0;
	}
}

void
rpipe_client_run(struct addrinfo *ap, int argc, char **argv)
{
	int fd;
	FILE *fp;
	char buf[IO_BUF_SIZE];
	int i;

	fd = socket(ap->ai_addr->sa_family, SOCK_STREAM, 0);
	if (fd == -1)
		error(EX_OSERR, errno, "can't create socket");

	if (connect(fd, ap->ai_addr, ap->ai_addrlen))
		error(EX_OSERR, errno, "connect");

	fp = fdopen(fd, "w+");

	for (i = 0; i < argc; i++) {
		if (i > 0)
			fputc(' ', fp);
		if (argv[i][strcspn(argv[i], " \"\\")] == 0)
			fprintf(fp, "%s", argv[i]);
		else {
			char *p;

			fputc('"', fp);
			for (p = argv[i]; *p; p++) {
				if (*p == '"' || *p == '\\')
					fputc('\\', fp);
				fputc(*p, fp);
			}
			fputc('"', fp);
		}
	}
	fputc('\r', fp);
	fputc('\n', fp);
	if (ferror(fp))
		error(EX_OSERR, 0, "write error");

	if (!fgets(buf, sizeof(buf), fp))
		error(EX_OSERR, errno, "error reading response");
	trim(buf);
	if (buf[0] == '-')
		error(EX_UNAVAILABLE, 0, "%s", buf + 2);
	else if (buf[0] != '+')
		error(EX_UNAVAILABLE, 0, "unexpected response: %s", buf);

	waitreply(fd);
}

void
rpipe_client(struct addrinfo *ap, int argc, char **argv)
{
	signal_setup(client_sig_handler, client_sigv, client_sigc);
	return rpipe_client_run(ap, argc, argv);
}

