/* This file is part of rpipe
   Copyright (C) 2018 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later
   <http://gnu.org/licenses/gpl.html>

   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/
#include <stdlib.h>
#include <unistd.h>

void error(int status, int err, char const *fmt, ...)
	__attribute__ ((__format__ (__printf__, 3, 4)));
void *xmalloc(size_t s);
char *xstrdup(char const *s);
void *xcalloc(size_t nmemb, size_t size);
void *xrealloc(void *p, size_t size);
void *x2nrealloc(void *p, size_t *pn, size_t s);


#define IO_BUF_SIZE 512

struct iobuf {
	char buf[IO_BUF_SIZE];    /* Initial line buffer */
	size_t start;             /* Start of data */
	size_t avail;             /* Start of available space */
};

enum crlfdot_encoder_state {
	crlfdot_encode_init,  /* initial state */
	crlfdot_encode_char,  /* Any character except \n */
	crlfdot_encode_lf,    /* prev. char was \n */
};

struct crlfdot_decoder_state {
	int state;
	int input;
};

#define CRLFDOT_DECODER_STATE_INIT { 0, 0 }

void crlfdot_encoder(enum crlfdot_encoder_state *state,
		     struct iobuf *ibuf, struct iobuf *obuf);
void crlfdot_decoder(struct crlfdot_decoder_state *st,
		     struct iobuf *obuf, struct iobuf *ibuf);

struct sockaddr;
char *sockaddr_str(struct sockaddr *sa, int salen);
struct addrinfo;
struct addrinfo *net_address_parse(char const *addrstr, int server);

struct runas {
	uid_t uid;
	gid_t gid;
	gid_t *gidv;
	size_t gidc;
	size_t gidn;
};

typedef struct runas runas_t;

runas_t *runas_parsev(char const *user_name, char const **grv, size_t grc);
static inline runas_t *
runas_parse(char const *user_name, char const *group_names)
{
	char const *grp[1] = { group_names };
	return runas_parsev(user_name, grp, 1);
}
void runas(runas_t *r);

struct privtab {
	struct privtab *next;
	char *command;
	runas_t *runas;
};

typedef struct privtab privtab_t;

runas_t *privtab_find(char const *command);
void privtab_read(char const *file);
void privtab_default(char const *user, char const *groups);


enum rpipe_server_type {
	rpipe_type_available,
	rpipe_type_listener, /* Listens for incoming requests */
	rpipe_type_init,
	/* Initializes the accepted connection, reads the first line,
	   starts the local command with the constructed command line */
	rpipe_type_net,
	/* Handles input/output with the net side of the pipe. */
	rpipe_type_progin,
	/* Pipes network input to the program */
	rpipe_type_progout,
	/* Pipes data from program's stdout to the network */
};

struct rpipe_server_common {
	enum rpipe_server_type type; /* Server type */
	char *str;                /* String representation */
	struct pollfd *pollfd;    /* File descriptor and configured events */
	union rpipe_server *next; /* Link to the next available server */
};

struct rpipe_server_listener {    /* Listener server */
	/* Common part */
	enum rpipe_server_type type; /* Server type */
	char *str;                /* String representation */
	struct pollfd *pollfd;    /* File descriptor and configured events */
	union rpipe_server *next; /* Link to the next available server */
	/* Server-specific part */
	int addrlen;              /* Length of the address */
	struct sockaddr *addr;    /* Address listened to */
};

struct rpipe_server_net {         /* Net I/O server. */
	/* Common part */
	enum rpipe_server_type type; /* Server type: rpipe_type_init or
					rpipe_type_net */
	char *str;                /* String representation */
	struct pollfd *pollfd;    /* File descriptor and configured events */
	union rpipe_server *next; /* Link to the next available server */
	/* Server-specific part */
	int addrlen;              /* Length of the address */
	struct sockaddr *addr;    /* Remote address */
	struct iobuf ibuf;        /* Input buffer */
	struct iobuf obuf;        /* Output buffer */
	/* Pointers to program input and output objects. */
	struct rpipe_server_progin *progin;
	struct rpipe_server_progout *progout;
};

enum prog_state {
	prog_startup,
	prog_running,
	prog_terminating
};

struct rpipe_server_progin {        /* Program input server */
	/* Common part */
	enum rpipe_server_type type; /* Server type */
	char *str;                /* String representation */
	struct pollfd *pollfd;    /* File descriptor and configured events */
	union rpipe_server *next; /* Link to the next server */
	/* Server-specific part */
	pid_t pid;                /* PID */
	struct iobuf buf;         /* I/O buffer */
	enum prog_state state;
	struct rpipe_server_net *net;
	struct rpipe_server_progout *out;
};

/* Status buffer contains
     CRLF                        2
     exit status (max. 65535)    5
     CRLF                        2
     .                           1
     CRLF                        2
*/
#define STATUS_BUFFER_SIZE 12

struct rpipe_server_progout {        /* Program output server */
	/* Common part */
	enum rpipe_server_type type; /* Server type */
	char *str;                /* String representation */
	struct pollfd *pollfd;    /* File descriptor and configured events */
	union rpipe_server *next; /* Link to the next server */
	/* Server-specific part */
	struct rpipe_server_progin *progin; /* Corresponding input server */
	struct rpipe_server_net *net;
	struct iobuf buf;         /* Input buffer */
	enum crlfdot_encoder_state encode_state;
	char status_buffer[STATUS_BUFFER_SIZE];
	size_t status_index;
};

typedef union rpipe_server {
	struct rpipe_server_common comm;
	struct rpipe_server_listener listener;
	struct rpipe_server_net net;
	struct rpipe_server_progin progin;
	struct rpipe_server_progout progout;
} rpipe_server_t;

void signal_setup(void (*handler)(int sig), int *sigv, int sigc);

void rpipe_client(struct addrinfo *, int argc, char **argv);
void rpipe_client_run(struct addrinfo *ap, int argc, char **argv);
void rpipe_server(struct addrinfo *, int np, int argc, char **argv);
void rpipe_test(int np, int argc, char **argv);
void report_prog_status(int status, char *prog);

extern int verbose;

