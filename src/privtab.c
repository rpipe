/* This file is part of rpipe
   Copyright (C) 2018-2021 Sergey Poznyakoff
   License GPLv3+: GNU GPL version 3 or later
   <http://gnu.org/licenses/gpl.html>
   
   This is free software: you are free to change and redistribute it.
   There is NO WARRANTY, to the extent permitted by law.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sysexits.h>
#include <errno.h>
#include "rpipe.h"
#include "wordsplit.h"

static privtab_t *privtab_head, *privtab_tail;
static runas_t *runas_privs;

runas_t *
privtab_find(char const *command)
{
	privtab_t *pt;
	for (pt = privtab_head; pt; pt = pt->next) {
		if (strcmp(pt->command, command) == 0)
			return pt->runas;
	}
	return runas_privs;
}

void
privtab_default(char const *user, char const *groups)
{
	runas_privs = runas_parse(user, groups);
}

void
privtab_read(char const *file)
{
	FILE *fp;
	char buf[512];
	int line = 0;
	size_t len;
	struct wordsplit ws;
	int wsflags = WRDSF_NOVAR | WRDSF_NOCMD | WRDSF_SQUEEZE_DELIMS
		      | WRDSF_ENOMEMABRT | WRDSF_SHOWERR | WRDSF_COMMENT;

	ws.ws_comment = "#";
	
	fp = fopen(file, "r");
	if (!fp)
		error(EX_NOINPUT, errno, "%s", file);
	while (fgets(buf, sizeof buf, fp)) {
		runas_t *ra;
		privtab_t *pt;
		
		++line;
		len = strlen(buf);
		if (len > 0 && buf[len-1] == '\n')
			buf[--len] = 0;
		else if (!feof(fp))
			error(EX_DATAERR, 0, "%s:%d: line too long",
			      file, line);
		if (wordsplit(buf, &ws, wsflags))
			break;
		wsflags |= WRDSF_REUSE;
		if (ws.ws_wordc == 0)
			continue;
		if (ws.ws_wordc < 2)
			error(EX_DATAERR, 0, "%s:%d: not enough fields",
			      file, line);
		ra = runas_parsev(ws.ws_wordv[1],
				  (char const **)(ws.ws_wordv + 1),
				  ws.ws_wordc - 1);
		pt = xcalloc(1, sizeof(*pt));
		pt->command = xstrdup(ws.ws_wordv[0]);
		pt->runas = ra;
		pt->next = NULL;
		if (privtab_tail)
			privtab_tail->next = pt;
		else
			privtab_head = pt;
		privtab_tail = pt;
	}
	fclose(fp);
}
		
			


	
