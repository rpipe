/*
  NAME
     t_sink - remote input sink for testing rpipe

  SYNOPSIS
     t_sink [-c] [-e CODE] [-f FILE] [-t MAP]

  DESCRIPTION
     The program reads input from stdin and writes it to FILE (if
     given), or to /dev/null.  It is intended to be run as rpipe
     server command, for testing.

  OPTIONS
     -c    Close stdout and stderr and re-open them to /dev/null.
     
     -e CODE
           Exit with the given status code.

     -f FILE
           Write output to FILE.  "-f -" means "write output to
	   stdout" (this cannot be used together with -c).
	   
	   By default, output is discarded.

     -t MAP
           Introduces delays at arbitrary points of output.  MAP is a
	   sequence of N=T pairs, separated by colons.  In each pair,
	   N is the offset and T is the delay.  The effect of each pair
	   is to sleep for T microseconds before outputting Nth character.
           T can be optionally folowed by 's', in which case it is measured
	   in seconds.

	   If a pair remains unconsumed when the input is exhaused, its T
	   part is taken as the time to sleep before closing the output
	   file.

	   At most 128 pairs can be supplied.

  EXIT STATUS
     On success, t_sink returns status code supplied with the -e option or
     0 by default.  On errors, 1 is returned.  On usage errors, 2 is returned.
	   
  LICENSE
     Copyright (C) 2020-2021 Sergey Poznyakoff

     This program is free software; you can redistribute it and/or modify it
     under the terms of the GNU General Public License as published by the
     Free Software Foundation; either version 3 of the License, or (at your
     option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License along
     with this program. If not, see <http://www.gnu.org/licenses/>.      
  
 */
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>
#include <string.h>

int
main(int argc, char **argv)
{
	FILE *ofile = NULL;
	int c;
	size_t n;
	int status = 0;
	int close_stdout = 0;
	
	struct timeout_map {
		size_t off;
		unsigned long delay;
	};
#define TMAX 128
	static struct timeout_map tmap[TMAX];
	static int tmax = 0;
	int tind = 0;
#define USEC_IN_SEC (1e6)

	while ((c = getopt(argc, argv, "ce:f:t:")) != EOF) {
		switch (c) {
		case 'c':
			close_stdout = 1;
			break;
			
		case 'e': {
			char *p;
			unsigned long n;

			n = strtoul(optarg, &p, 10);
				
			if ((n == 0 && errno == ERANGE) || n > 127) {
				fprintf(stderr, "invalid exit status\n");
				return 2;
			}
			status = n;
			break;
		}
			
		case 'f':
			if (strcmp(optarg, "-") == 0)
				ofile = stdout;
			else if ((ofile = fopen(optarg, "w")) == NULL) {
				perror(optarg);
				return 1;
			}
			break;

		case 't':
			while (*optarg) {
				char *p;
				unsigned long n;

				if (tmax == TMAX) {
					fprintf(stderr, "timeout map overflow\n");
					return 2;
				}
				
				n = strtoul(optarg, &p, 10);
				
				if (n == 0 && errno == ERANGE) {
					fprintf(stderr, "invalid offset (near %s)\n",
						optarg);
					return 2;
				}
				tmap[tmax].off = n;

				optarg = p;
				if (*optarg == 0)
					break;
				if (*optarg != '=') {
					fprintf(stderr, "expected '=' (near %s)\n",
						optarg);
					return 2;
				}
				optarg++;

				n = strtoul(optarg, &p, 10);
				if (n == 0 && errno == ERANGE) {
					fprintf(stderr, "invalid delay (near %s)\n",
						optarg);
					return 2;
				}
				if (*p == 's') {
					if (UINT_MAX / USEC_IN_SEC < n) {
						fprintf(stderr, "delay too big (near %s)\n",
							optarg);
						return 2;
					}
					n *= USEC_IN_SEC;
					p++;
				}
				tmap[tmax].delay = n;
				tmax++;

				optarg = p;
				if (*optarg == ':')
					optarg++;
			}
			break;	
			
		default:
			return 2;
		}
	}

	if (!ofile && (ofile = fopen("/dev/null", "w")) == NULL) {
		perror("/dev/null");
		return 1;
	}	

	if (close_stdout) {
		if (ofile == stdout) {
			fprintf(stderr, "%s: '-c' and '-f -' used together\n",
				argv[0]);
			return 2;
		}
		fclose(stdout);
		if ((stdout = fopen("/dev/null", "w")) == NULL) {
			perror("/dev/null");
			return 1;
		}
		fclose(stderr);
		stderr = stdout;
	}
	
	for (n = 0; (c = getchar()) != EOF; n++) {
		if (tind < tmax && tmap[tind].off == n) {
			usleep(tmap[tind].delay);
			tind++;
		}
		fputc(c, ofile);
	}
	
	if (tind < tmax)
		usleep(tmap[tind].delay);

	fclose(ofile);
	    
	return status;
}

	
