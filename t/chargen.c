/*
  NAME
     chargen - generate a stream of characters
     
  SYNOPSIS
     chargen [-c C] [-l LEN] [-p] [-s N] [-t MAP]

  DESCRIPTION
     Produces on standard output a stream of characters.  The stream consists
     of all 256 characters repeated cyclically until total number of characters
     reaches 4096 (or LEN).
     
  OPTIONS
     -c C  Start from ASCII character C

     -l LEN
           Stop when LEN characters have been generated.
     
     -p    Produce only printable characters

     -s N  Start from character with ordinal number N

     -t MAP
           Introduces delays at arbitrary points of output.  MAP is a
	   sequence of N=T pairs, separated by colons.  In each pair,
	   N is the offset and T is the delay.  The effect of each pair
	   is to sleep for T microseconds before outputting Nth character.
           T can be optionally folowed by 's', in which case it is measured
	   in seconds.

	   At most 128 pairs can be supplied.
	   
  LICENSE
     Copyright (C) 2020-2021 Sergey Poznyakoff

     This program is free software; you can redistribute it and/or modify it
     under the terms of the GNU General Public License as published by the
     Free Software Foundation; either version 3 of the License, or (at your
     option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License along
     with this program. If not, see <http://www.gnu.org/licenses/>. 
     
*/
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>

int
main(int argc, char **argv)
{
	int c;
	int start = 0;
	int mod = UCHAR_MAX;
	size_t i, len = 4096;
	int printable = 0;
	char *p;
	
	struct timeout_map {
		size_t off;
		unsigned long delay;
	};
#define TMAX 128
	static struct timeout_map tmap[TMAX];
	static int tmax = 0;
	int tind = 0;
#define USEC_IN_SEC (1e6)
	
	while ((c = getopt(argc, argv, "cl:ps:t:")) != EOF) {
		switch (c) {
		case 'c':
			start = optarg[0];
			break;
		case 'p':
			printable = 1;
			break;
		case 's':
			start = atoi(optarg) % UCHAR_MAX;
			break;
		case 'l':
			errno = 0;
			len = strtoul(optarg, &p, 10);
			if (*p) {
				fprintf(stderr, "bad length (near %s)\n", p);
				exit(1);
			} else if (errno) {
				perror("bad length");
				exit(1);
			}
			break;
		case 't':
			while (*optarg) {
				char *p;
				unsigned long n;

				if (tmax == TMAX) {
					fprintf(stderr, "timeout map overflow\n");
					exit(1);
				}
				
				n = strtoul(optarg, &p, 10);
				
				if (n == 0 && errno == ERANGE) {
					fprintf(stderr, "invalid offset (near %s)\n",
						optarg);
					exit(1);
				}
				tmap[tmax].off = n;

				optarg = p;
				if (*optarg == 0)
					break;
				if (*optarg != '=') {
					fprintf(stderr, "expected '=' (near %s)\n",
						optarg);
					exit(1);
				}
				optarg++;

				n = strtoul(optarg, &p, 10);
				if (n == 0 && errno == ERANGE) {
					fprintf(stderr, "invalid delay (near %s)\n",
						optarg);
					exit(1);
				}
				if (*p == 's') {
					if (UINT_MAX / USEC_IN_SEC < n) {
						fprintf(stderr, "delay too big (near %s)\n",
							optarg);
						exit(1);
					}
					n *= USEC_IN_SEC;
					p++;
				}
				tmap[tmax].delay = n;
				tmax++;

				optarg = p;
				if (*optarg == ':')
					optarg++;
			}
			break;	
			
		default:
			exit(2);
		}
	}

	if (printable && !isprint(start))
		do {
			start = (start + 1) % mod;
		} while (printable && !isprint(start));
	
	for (i = 0; i < len; i++) {
		if (tind < tmax && tmap[tind].off == i) {
			usleep(tmap[tind].delay);
			tind++;
		}
		putchar(start);
		do {
			start = (start + 1) % mod;
		} while (printable && !isprint(start));
	}
	return 0;
}
